#!/usr/bin/env bash

killall ubuntuone-login ubuntuone-preferences ubuntuone-syncdaemon
sudo rm -rf ~/.local/share/ubuntuone
rm -rf ~/.cache/ubuntuone
rm -rf ~/.config/ubuntuone

sudo apt-get purge ubuntuone-client python-ubuntuone-storage*
sudo apt-get remove unity-lens-shopping
sudo apt-get remove python-ubuntu-sso-client
