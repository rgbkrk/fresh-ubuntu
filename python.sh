#!/usr/bin/env bash

# IPython
sudo apt-get install ipython ipython-doc ipython-notebook
sudo apt-get install ipython3 ipython3-notebook

# Scientific Computing
sudo apt-get install python-numpy python-scipy
sudo apt-get install python-sklearn python-sklearn-doc

# Scraping
sudo apt-get install python-scrapy

# Deployment
sudo apt-get install python-setuptools python-setuptools-git
sudo apt-get install fabric
